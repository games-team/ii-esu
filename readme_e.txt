ES(D version) readme_e.txt
ver1.0a
HIZ

*** control

push center to start
push lower right to BGM ON/OFF

Ship moves toward cursor.
Auto lock(release button to unlock.)

Small circle represent a area your ship don't move and shield gauge.
Large circle represent a area your ship move slowly and extend gauge.
Outside circle represent a area your ship can move and level gauge.


- hide window     　　　　　 F5 key
- soft reset     　　　　　　 ENTER key
- quit　　　　　　　　　　 ESC key 


*** Comments

If you have any comments, please mail to hiz_oka@yahoo.co.jp

*** Acknowledgement

ES is written in the D Programming Language(ver. 1.005).
Simple DirectMedia Layer is used for media handling.
SDL_mixer and Ogg Vorbis CODEC are used for playing BGMs/SEs.
D Header files at D - porting are for use with OpenGL, SDL and SDL_mixer.
Mersenne Twister is used for creating a random number.
ABA's game sources are refered.
kenmo's introduction to D program language (http://www5.atwiki.jp/yaruhara/pages/74.html) are refered.

sound materials

-musics
HumanPark(http://www.human-park.net/index.htm)

*** Change log
2007/07/15 ver1.0a
・corrected the behavior on pause.
2007/07/14 ver1.0
・released.
