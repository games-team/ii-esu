module es.field;

private import opengl;
private import std.math;
private import es.gamemanager;
private import es.basis;


public class Field {
  public real x ,y ,z;
  public real radius;
   public this(){
    start();
  }
   public void start(){
    radius = 400;
    x = y = z = 0;
  }
   public void move(){
   }
   public void draw(){
   	glPushMatrix();
    glColor3d(1,1,1);
    glLineWidth((stage.level - 1) / 100 + 1);
    glBegin(GL_LINE_LOOP);
   //
    if(0 < stage.level && (stage.level - 1) % 100 != 0){
      glColor3d(1,1,1);
      arc(0 ,0 ,radius ,radius ,(PI  / 2.0) ,(PI  / 2.0 - 2.0 * PI / 100 * ((stage.level - 1) % 100) ), PI / 180.0 * 2.0);
      glColor3d(0.5,0.5,0.5);
      arc(0 ,0 ,radius  ,radius ,(PI  / 2.0 - 2.0 * PI  / 100 * ((stage.level - 1) % 100) ) ,(PI / 2 - 2.0 * PI), PI / 180.0 * 2.0);
    }else{
      glColor3d(0.5,0.5,0.5);
      arc(x ,y ,radius  ,radius ,0.0,PI * 2.0,  PI / 180.0 * 2.0);
    }
    glLineWidth(1);
    //ellipse(x ,y ,radius * 2 ,radius * 2);
	glPopMatrix();
  }
   public real transX(real x,real y ,real z){
    return this.x + x;
  }
   public real transY(real x,real y ,real z){
    return this.y + y;
  }
  public  real transZ(real x,real y ,real z){
    return this.z + z;
  }
}
