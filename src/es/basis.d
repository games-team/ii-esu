module es.basis;
private import opengl;
private import std.math;



public void arc(real x, real y, real rx, real ry, real srad = 0.0, real erad = PI * 2.0, real drad = PI / 180.0 * 5.0){
	glBegin(GL_LINE_STRIP);
	if(srad < erad){
		for(real rad=srad;rad<erad+drad;rad += drad){
			glVertex3d(x + rx * cos(rad), y + ry * sin(rad), 0.0);
		}
	}else{
			for(real rad=srad;rad>erad-drad;rad -= drad){
			glVertex3d(x + rx * cos(rad), y + ry * sin(rad), 0.0);
		}
	}
	
	
	glEnd();
}
public void fillArc(real x, real y, real rx, real ry, real srad = 0.0, real erad = PI * 2.0, real drad = PI / 180.0 * 5.0){
	glBegin(GL_POLYGON);
	glVertex3d(x, y, 0.0);
	if(srad < erad){
		for(real rad=srad;rad<erad+drad;rad += drad){
			glVertex3d(x + rx * cos(rad), y + ry * sin(rad), 0.0);
		}
	}else{
			for(real rad=srad;rad>erad-drad;rad -= drad){
			glVertex3d(x + rx * cos(rad), y + ry * sin(rad), 0.0);
		}
	}
	
	
	glEnd();
}

public real dist(real x0, real y0, real z0, real x1, real y1, real z1){
	
	return sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0)+(z1-z0)*(z1-z0));
}

public real dist(real x0, real y0, real x1, real y1){
	
	return sqrt((x1-x0)*(x1-x0)+(y1-y0)*(y1-y0));
}

public real constrain(real x, real min, real max){
	real result = x;
	if(result < min)result = min;
	if(max < result)result = max;
	return result;
}
/*
private enum{
	ALLOW ,HAND
}
private uint cursorshape = ALLOW;


public void setHandCursor(){
	 
	if(cursorshape != HAND){
		HCURSOR hand = cast(HCURSOR)LoadImage(null, cast(LPSTR)(cast(DWORD)(cast(WORD)(IDC_HAND))), IMAGE_CURSOR,0, 0, LR_DEFAULTSIZE | LR_SHARED);
		
		SetCursor(hand);
		cursorshape = HAND;
	}
}
*/