module es.sound;

private import SDL_mixer;
private import SDL;

public enum music_kind{
	MUSIC1 
};
private int _Sound_PlayingMusic;
private bool _nosound = false;


Mix_Music   *music1;


public void Sound_init(){
	SDL_InitSubSystem(SDL_INIT_AUDIO);
	int audio_rate;
    Uint16 audio_format;
    int audio_channels;
    int audio_buffers;
	audio_rate = 44100;
    audio_format = AUDIO_S16;
    audio_channels = 2;
    audio_buffers = 2048;
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers) < 0){
		_nosound = true;
		return ;
	}
	Mix_QuerySpec(&audio_rate, &audio_format, &audio_channels);
	
	music1 = Mix_LoadMUS("music/loop1.ogg");
	
	_Sound_PlayingMusic = -1;
}

public void Sound_PlayMusic(int kind ,int loop = -1){
	if(!nosound){
		if(Sound_PlayingMusic == kind)return;
		
		switch(kind){
			case music_kind.MUSIC1:Mix_PlayMusic(music1, loop);break;
			default:return;
		}
		_Sound_PlayingMusic = kind;
	}
}

public void Sound_HaltMusic(){
	Mix_HaltMusic();
	_Sound_PlayingMusic = -1;
}
public void Sound_FadeOutMusic(int ms){
	Mix_FadeOutMusic(ms);
	_Sound_PlayingMusic = -1;
}

public void Sound_free(){
	Mix_FreeMusic(music1);

}
public void nosound(bool s){
	_nosound = s;
	if(nosound){
		Sound_HaltMusic();
		
	}
}
public bool nosound(){
	return _nosound;
}

public int Sound_PlayingMusic(){
	if(Mix_PlayingMusic() == 0)_Sound_PlayingMusic = -1;
	return _Sound_PlayingMusic;
}