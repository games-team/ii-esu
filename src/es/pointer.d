module es.pointer;

private import opengl;
private import std.math;
private import es.basis;
private import es.gamemanager;
private import es.shape;
private import es.ship;
private import es.screen;

public class Pointer {
  real x,y,z;
  real fx ,fy ,fz;
  private Shape shape;
   public this(){
    start();
    real[] a = [5 ,10 ,20];
    real[] b = [0 ,5 ,0];
    shape = new Pole(a ,b ,2);
    //shape = new Symmentry(s,4);
  }
   public void start(){
    x = 0;
    y = 0;
    z = 0;
    fx = 0;
    fy = 0;
    fz = 0;
  }
   public void move(){
   	
    fx = mousex + ship.x - screen.SCREEN_WIDTH / 2;
    fy = - mousey + ship.y + screen.SCREEN_HEIGHT / 2;
    fz = 0;
	x = field.transX(fx ,fy ,fz);
    y = field.transY(fx ,fy ,fz);
    z = field.transZ(fx ,fy ,fz);
	
   }
   public void draw(){
    
    real[] ax = shape.x;
    real[] ay = shape.y;
    real[] az = shape.z;
    real r = dist(x ,y ,z ,ship.x ,ship.y ,ship.z);
	
	
	glPushMatrix();
    glColor3d(0.5, 0.5, 0.5);
	glBegin(GL_LINES);
	
	if(r>1e-6){
//		glVertex3d(0,0,0);
//		glVertex3d(0,10,0);
		glVertex3d(ship.x ,ship.y ,ship.z);
		glVertex3d((x - ship.x) / r * screen.SCREEN_WIDTH + ship.x,(y - ship.y) / r * screen.SCREEN_HEIGHT + ship.y,(z - ship.z) / r * screen.SCREEN_WIDTH / 2 + ship.z);
	}
	 glEnd();
	
    glPopMatrix();
	glPushMatrix();
	
	
	glColor3d(1, 1, 1);
    glLineWidth(2);
		 
    glTranslated(x ,y ,z);
	
    glRotated(cast(real)GameManager.count * 3.0, 0, 0, 1);
    
    glScaled(2.0,2.0,2.0);
    for(int i = 0;i < 3;i ++){
      glBegin(GL_LINE_LOOP);
      for(int j = 0; j < ax.length ;j ++){
        glVertex3d(ax[j] ,ay[j] ,az[j]);
      }
      glEnd();
      glRotated(120.0, 0, 0, 1);
    }
    glLineWidth(1);
	
	glPopMatrix();
  }
}
