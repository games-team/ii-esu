module es.mainloop;
private import opengl;
private import SDL;
private import es.gamemanager;

private import es.mouse;

public class Mainloop{
	

	static int count;

	private:

	
	public:
	const int INTERVAL = 33;
	const int MAX_SKIP_FRAME = 5;
	GameManager gm;
	Mouse mouse;
	
	bool accframe;
	bool active;
	uint interval;
	uint maxSkipFrame;
	
	public this(Mouse mouse){
		this.mouse = mouse;
	}
	public void loop(){
		gm = new GameManager(mouse);
		
		long prvTickCount = 0;
		long nowTick;
		int frame;
		
		//partsManager = new PartsManager(128);
		gm.start();
		setActive(true);
		accframe = false;
		interval = INTERVAL;
		maxSkipFrame = MAX_SKIP_FRAME;
		SDL_Event event;
		SDLKey *sdlkey;
		Uint32 prevTime;
		//count = 0;
		
		bool done = false;
		//for(int iCount = 0 ; iCount < 128 ; iCount++) mask[iCount] = 0xF0;
		while(!done){
			//prevTime = SDL_GetTicks();
			
			//SDL_UpdateRect(gScreenSurface,0,0,SCREEN_WIDTH,SCREEN_HEIGHT);
			
			while(SDL_PollEvent(&event)){
				//event.type = SDL_USEREVENT;
				
				
				switch(event.type){
					case SDL_QUIT:
						done = true;
						break;

					case SDL_ACTIVEEVENT:
						switch(event.active.state){
						
						case SDL_APPACTIVE:
							if(event.active.gain){
								//active = true;
							}else setActive(false);
							break;
							
						case SDL_APPINPUTFOCUS:
							if(event.active.gain){
								//active = true;
							}else setActive(false);
							
							break;
						
						case SDL_APPMOUSEFOCUS:
//							if(event.active.gain)active = true;
//							else 
							setActive(false);
							break;
							
						default:break;
						}
						break;
						
					case SDL_MOUSEBUTTONDOWN:
						setActive(true);
						break;
					
					case SDL_KEYDOWN:
						
						
						sdlkey=&(event.key.keysym.sym);
						if(*sdlkey==SDLK_ESCAPE){
							done = true;
						}//else if(*sdlkey==SDLK_F9)screen.toggleFullScreen();
						else if(*sdlkey==SDLK_F5)SDL_WM_IconifyWindow();    
						else if(*sdlkey==SDLK_RETURN){
							gm.returnPushed();
						}
						break;
						
						default:break;
				}
			}
			
			
			nowTick = SDL_GetTicks();
			frame = cast(int) (nowTick-prvTickCount) / interval;
			
			if(frame <= 0){
				frame = 1;
				SDL_Delay(cast(uint)(prvTickCount+interval-nowTick));
				if (accframe) {
					prvTickCount = SDL_GetTicks();
				} else {
					prvTickCount += interval;
				}
			
			}else if (frame > maxSkipFrame) {
				frame = maxSkipFrame;
				prvTickCount = nowTick;
			} else {
				prvTickCount += frame * interval;
			}
			
			
			screen.clear();
			if(active){
				for (int i = 0; i < frame; i++) {
					gm.move();
				}
			}
			gm.draw();
			screen.flip();
			
		}
		
		gm.close();
		SDL_Quit();
	}
	private void setActive(bool act){
	active = act;
	if(active){
//		SDL_ShowCursor(SDL_DISABLE);
	}else{
		SDL_ShowCursor(SDL_ENABLE);
	}
}
	
}

