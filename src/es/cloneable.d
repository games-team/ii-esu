module es.cloneable;
interface Cloneable{
	public Object clone();
}