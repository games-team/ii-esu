module es.stage;

private import std.math;
private import es.basis;
private import es.rand;
private import es.gamemanager;
private import es.screen;
private import es.enemy;
private import es.moving;

public class Stage{
  int level;
  int levCnt;
  real wait;
  real rank;
  real delay;
  public void start(int level){
    this.level = level;
    wait = 60;
    delay = 0;
    levCnt = 0;
    setRank(level);
  }
  public void update(){
    
    if(wait < delay){
      int[][] eLevList =[
        [1 ,2],
        [0 ,5],
        [3 ,6],
        [4]
      ];
      real eLev = (rank - 1) * 10;
      real w = 0;
      while(wait < (rank - 1) * 500.0){
        int lv = cast(int)floor(rand.nextFloat(constrain(eLev,0 ,eLevList.length)));
        while(eLevList[lv].length == 0){
          lv --;
          if(lv < 0)return;
        }
        int type = eLevList[lv][cast(int)floor(rand.nextFloat(eLevList[lv].length))];
        real theta = rand.nextFloat(2.0*PI);
        real aim = rand.nextFloat(2.0*PI);
      //ep.addEnemy(null ,type ,new Homing(TWO_PI / 180) ,100 ,cos(theta) * (field.radius - 100) ,sin(theta) * (field.radius - 100) ,-400 ,- cos(aim) * 5.0 ,- sin(aim) * 5.0);
        switch(type){
          case 0:
          ep.addEnemy(null ,ICEBERG ,new Dart(2.0*PI / 180.0) ,20 ,cos(theta) * (field.radius - 200) ,sin(theta) * (field.radius - 200) ,-400 ,- cos(theta) * 5.0 ,- sin(theta) * 5.0);
          w = 40.0;
          break;
          case 1:
          ep.addEnemy(null ,NIGHTWING ,new Homing(2.0*PI / 180) ,80 ,cos(theta) * (field.radius - 100) ,sin(theta) * (field.radius -100) ,-400 ,- cos(aim) * 5.0 ,- sin(aim) * 5.0);
          w = 200.0;
          break;
          case 2:
          ep.addEnemy(null ,PLANE ,new Dart(2.0*PI / 90) ,30 ,cos(theta) * (field.radius + 400) ,sin(theta) * (field.radius + 400) ,0 ,- cos(theta) * 6.0 ,- sin(theta) * 6.0);
          w = 80.0;
          break;
          case 3:
          ep.addEnemy(null ,THREEWAY ,new Homing(2.0*PI / 180) ,60 ,cos(theta) * (field.radius - 100) ,sin(theta) * (field.radius - 100) ,-400 ,- cos(aim) * 5.0 ,- sin(aim) * 5.0);
          w = 200.0;
          break;
          case 4:
          ep.addEnemy(null ,FOURWAY ,new Homing(2.0*PI / 180) ,80 ,cos(theta) * (field.radius - 100) ,sin(theta) * (field.radius - 100) ,-400 ,- cos(aim) * 5.0 ,- sin(aim) * 5.0);
          w = 200.0;
          break;
          case 5:
          ep.addEnemy(null ,SCATTER ,new Homing(2.0*PI / 180) ,80 ,cos(theta) * (field.radius - 100) ,sin(theta) * (field.radius - 100) ,-400 ,- cos(theta) * 5.0 ,- sin(theta) * 5.0);
          w = 200.0;
          break;
          case 6:
          ep.addEnemy(null ,SWINE ,new Dart(2.0*PI / 45) ,10 ,cos(theta) * (field.radius + 400) ,sin(theta) * (field.radius + 400) ,0 ,- cos(theta) * 9.0 ,- sin(theta) * 9.0);
          w = 30.0;
          break;
		  default:break;
        }
        wait += w;
        eLev -= w / 100;
        if(rand.nextFloat(1) < 0.5)break;
      }
    }
    wait -= sqrt(rank);
    delay = (1 - rand.nextFloat(2)) * rank * 30;
    levCnt ++;
    if(180 < levCnt){
      level ++;
      setRank(level);
      levCnt = 0;
    }
  }
  public void setRank(int level){
    int lev1 = (level - 1) % 10;
    int lev10 = (level - 1) / 10;
    rank = 1 + cast(real)lev1 * (0.02 + cast(real)lev10 * 0.001) + cast(real)lev10 * 0.04;
    //System.out.println(rank);
  }
}
