module es.moving;

private import std.math;
private import es.basis;
private import es.gamemanager;
private import es.enemy;



public abstract class Moving {
  public Enemy e;
  public this(){}
  public void set(Enemy enemy){
    this.e = enemy;
    start();
  }
  public void start(){}
  public abstract void update(bool attacking);
}


public class Straight:Moving{
  public void update(bool attacking){
    if(attacking ==false){
      e.fx += e.vx;
      e.fy += e.vy;
    }
  }
}
public class Homing:Moving{
  public real si,co;
  public this(real drad){
    super();
    si = sin(drad);
    co = cos(drad);
  }
  
  public void update(bool attacking){
  	
    real temp;
	real in1 = (ship.fx - e.fx) * -e.vy + (ship.fy - e.fy) * e.vx;
	
    if(in1 < 0){
      temp = e.vx;
      e.vx = temp * co + e.vy * si;
      e.vy = - temp * si + e.vy * co;
      
    }else{      
      temp = e.vx;
      e.vx = temp * co - e.vy * si;
      e.vy = temp * si + e.vy * co;
      
    }
	
    if(attacking ==false){
      e.fx += e.vx;
      e.fy += e.vy;
    }
	
    
  }
  
}
public class Dart:Moving{
  public real si,co;
  public this(real drad){
    super();
    si = sin(drad);
    co = cos(drad);
  }
  public void update(bool attacking){
    if(attacking ==false){
      e.fx += e.vx;
      e.fy += e.vy;
    }else{
      real temp;
      real in1 = (ship.fx - e.fx) * -e.vy + (ship.fy - e.fy) * e.vx;
      if(in1 < 0){
        temp = e.vx;
        e.vx = temp * co + e.vy * si;
        e.vy = - temp * si + e.vy * co;
      
      }else{      
        temp = e.vx;
        e.vx = temp * co - e.vy * si;
        e.vy = temp * si + e.vy * co;
      
      }
    }
    
    
  }
}
